package net.techu.data;

public class Product {

    public String id;
    public String nombre;
    public double precio;
    public String categoria;

    public Product(String id, String nombre,
                    double precio,
                    String categoria) {
        this.id = id;
        this.nombre = nombre;
        this.precio = precio;
        this.categoria = categoria;
    }

}
