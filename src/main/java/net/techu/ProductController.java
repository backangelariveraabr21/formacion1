package net.techu;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.models.Response;
import net.techu.data.Product;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;

@RestController
public class ProductController {

    private ArrayList<Product> listaProductos;

    public ProductController() {
        listaProductos = new ArrayList<Product>();
        listaProductos.add(new Product("ABC", "Producto ABC", 123.22, "CAT1"));
        listaProductos.add(new Product("DEF", "Producto DEF", 321.99, "CAT2"));
        listaProductos.add(new Product("GHI", "Producto GHI", 444.73, "CAT3"));
    }

    @GetMapping("/productos")
    public ResponseEntity<ArrayList<Product>> getListaProduct() {
        return new ResponseEntity<ArrayList<Product>>(listaProductos, HttpStatus.OK);
    }

    @PostMapping("/productos")
    @ApiOperation(value="Crear producto", notes="Este método crea un producto")
    public ResponseEntity<Product> addProduct(@ApiParam(name="producto",
            type="Producto",
            value="producto a crear",
            example="PR1",
            required = true) @RequestBody Product productNuevo)
    {
        listaProductos.add(productNuevo);
        return new ResponseEntity<Product>(productNuevo, HttpStatus.CREATED);
    }

    @PostMapping("/productos/byname")
    @ApiOperation(value="Crear producto por nombre", notes="Este método crea un producto solo con el nombre")
    public ResponseEntity<Product> addProductByName(@ApiParam(name="nombre",
            type="String",
            value="nombre del producto a crear",
            example="PR1",
            required = true) @RequestParam String nombre)
    {
        Product productNuevo = new Product("123", nombre, 122, "CATX");
        listaProductos.add(productNuevo);
        return new ResponseEntity<Product>(productNuevo, HttpStatus.CREATED);
    }

}
